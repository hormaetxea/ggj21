﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public int narioSize;
    public GameObject preFabNario;
    public GameObject preFabWalls;
    public float ordertime = 5f;
    public Mesh NarioMesh;
    public Mesh UoadMesh;
    public Mesh CowserMesh;

    float elapsedTime = 0f;

    public bool IsOnMenu = true;
    public bool IsInitGame = false;
    public bool IsGameRunning = true;
    public bool IsGameLost = false;
    public bool IsGameWin = false;
    
    public int ScanAttemps = 3;

    List<GameObject> allNario = new List<GameObject>();

    AudioSource source;
    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        elapsedTime += Time.deltaTime;

        if (IsInitGame)
        {
            AddLevel();
            AddNarios();
            SetBehavior();
            SetImitating();
            IsInitGame = false;
        }
        if (IsGameRunning)
        {
            source.Play();
        }

        if (elapsedTime > ordertime)
        {
            elapsedTime = 0;
            SetBehavior();
            SetImitating();
        }
        
    }
    void AddLevel()
    {
        GameObject walls = Instantiate(preFabWalls, new Vector3(0, 0, 0), Quaternion.identity);
        walls.transform.localScale = new Vector3(preFabWalls.transform.localScale.x * narioSize / 100, preFabWalls.transform.localScale.y, preFabWalls.transform.localScale.z * narioSize / 100);
    }
    void AddNarios()
    {
        for (int i = 0; i < narioSize; i++)
        {
            allNario.Add(Instantiate(preFabNario, new Vector3(0, 0, 0), Quaternion.identity));
            if (allNario[i] != null)
            {
                var inputR = allNario[i].GetComponent<InputResponse>();

                if (i == 0)
                    inputR.BehaviorType = 1;

                else if (i > narioSize / 2 + narioSize / 4)
                    inputR.BehaviorType = 2;

                else if (i > narioSize / 2)
                    inputR.BehaviorType = 3;
                else
                    inputR.BehaviorType = 4;

                switch (Random.Range(0,3))
                {

                    case 0:
                        allNario[i].GetComponentInChildren<SkinnedMeshRenderer>().sharedMesh = CowserMesh;
                        break;

                    case 1:
                        allNario[i].GetComponentInChildren<SkinnedMeshRenderer>().sharedMesh = UoadMesh;
                        break;
                   
                }
            }

        }
    }
    void SetBehavior()
    {

        for (int i = 1; i < narioSize/20; i++)
        {
            allNario[i].GetComponent<InputResponse>().BehaviorType = Random.Range(2, 4);
        }
    }
    void SetImitating()
    {

        for (int i = 0; i < narioSize; i++)
        {
            var inputR = allNario[i].GetComponent<InputResponse>();

            if (inputR.BehaviorType == 2)
                inputR.ImitatingGO = allNario[0];
            else if(inputR.BehaviorType == 3)
            {
                inputR.ImitatingGO = allNario[Random.Range(0, narioSize)];
            }
            else
                inputR.ImitatingGO = allNario[i];


        }
    }

    public GameObject GetNewTarget()
    {
        return allNario[Random.Range(0, narioSize)];
    }

    public void OnGameObjectDeleted(GameObject obj)
    {
        if (obj == allNario[0])
        {
            IsGameLost = true;
            return;
        }

        narioSize--;
        allNario.Remove(obj);
        Destroy(obj);
    }

    public void OnGameObjectScanned(GameObject obj)
    {
        if (obj == allNario[0])
            IsGameWin = true;
        else
            ScanAttemps--;

        if (ScanAttemps == 0)
            IsGameLost = true;
        
    }

    void InstantiatePrefabs()
    {

    }
}
