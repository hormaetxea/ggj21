﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float duration;    //the max time of a walking session (set to ten)
    float elapsedTime = 0f; //time since started walk

    float randomX;  //randomly go this X direction
    float randomZ;  //randomly go this Z direction

    public float MovementSpeed = 1f;

    public bool IsDragged;

    Rigidbody rb;




    void Start()
    {
        randomX = Random.Range(-MovementSpeed, MovementSpeed);
        randomZ = Random.Range(-MovementSpeed, MovementSpeed);
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {

        elapsedTime += Time.deltaTime;
        if (IsDragged)
        {
            rb.constraints = RigidbodyConstraints.FreezeAll;
        }
        else
        {
            rb.constraints = RigidbodyConstraints.None;
            rb.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
            return;
        if (collision.gameObject.CompareTag("Wall"))
        {
            randomX *= -1.5f;
            randomZ *= -1.5f;
            return;
        }
        
        Vector3 direction = (transform.position - collision.transform.position).normalized;
        randomX = direction.x;
        randomZ = direction.z;
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
            return;
        if (collision.gameObject.CompareTag("Wall"))
        {
            randomX = Random.Range(-MovementSpeed, MovementSpeed);
            randomZ = Random.Range(-MovementSpeed, MovementSpeed);
            return;
        }
        elapsedTime = 0;
        
        Vector3 direction = (transform.position - collision.transform.position).normalized;
        randomX = direction.x;
        randomZ = direction.z;
        if(rb.velocity == new Vector3())
        {
            randomX = Random.Range(-MovementSpeed, MovementSpeed);
            randomZ = Random.Range(-MovementSpeed, MovementSpeed);
        }
    }


    void FixedUpdate()
    {
  
        rb.velocity = new Vector3(randomX, 0, randomZ);
    
    }

}