﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class ButtonBehavior : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    Button button;
    public DragManager drag;
    public int type = 0;

    public Animator anim;

    public bool ActiveButton = false;

    // Start is called before the first frame update
    void Start()
    {
        button = this.GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("Hover", ActiveButton);
    }

    public void OnPointerEnter(PointerEventData p)
    {
        if (drag.dragging)
        {            
            Debug.Log("OPEN TRASHCAN");
            drag.ActivatedButton = this;
            ActiveButton = true;
        } 
    }

    public void OnPointerExit(PointerEventData p)
    {
        if (ActiveButton)
        {
            drag.ActivatedButton = null;
            Debug.Log("NOT TODAY");
        }

        ActiveButton = false;
 
    }
}
