﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameratest : MonoBehaviour
{
    public CameraMovement mov;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition, Camera.main.stereoActiveEye);
            int mask = LayerMask.GetMask("Floor");

            if (Physics.Raycast(ray, out hit, 10000, mask))
            {
                this.transform.position = hit.point;
            }
        }

        if (mov.dragging)
        {
            this.GetComponent<MeshRenderer>().material.color = Color.red;
            this.transform.position = mov.dropPosition;
        }
        else
            this.GetComponent<MeshRenderer>().material.color = Color.white;
    }
}
