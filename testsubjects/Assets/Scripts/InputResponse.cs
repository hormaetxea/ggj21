﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputResponse : MonoBehaviour
{
    //1 - Player
    //2 - Imitates player
    //3 - Imitates another obj
    //4 - does hes own thing
    public int BehaviorType;

    public GameObject ImitatingGO = null;
    
    public Animator anim;
    public int Action;

    float elapsedTime;
    List<int> actionTracker;
    Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        elapsedTime += Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.isKinematic = true;
        }
        else if(Input.GetKeyUp(KeyCode.Space))
            rb.isKinematic = false;

        //ActualPlayer();
        switch (BehaviorType)
        {
            case 1:
                ActualPlayer();
                break;

            case 2:
                ImitatingPlayer();
                break;

            case 3:
                ImitatingPlayer();
                break;

            case 4:     
                ImMyOwnBoss();
                break;

            default:
                break;
        }
    }
    void ActualPlayer()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Action = 1;
            anim.SetTrigger("IsFlip");
            rb.isKinematic = true;
            //Do action
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            Action = 2;
            anim.SetTrigger("IsIdle");
            rb.isKinematic = true;
            //Do action
        }
        /*else if (Input.GetKeyDown(KeyCode.S))
        {
            //Do action
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            //Do action
        }*/
        else
        {
            Action = 0;
            rb.isKinematic = false;
        }
    }
    void ImitatingPlayer()
    {
        if (ImitatingGO == null)
        {
            ImitatingGO = GameObject.Find("GameManager").GetComponent<GameManager>().GetNewTarget();
        }

        var imit = ImitatingGO.GetComponent<InputResponse>();

        switch (imit.Action)
        {
            case 1:
                anim.SetTrigger("IsFlip");
                break;

            case 2:
                anim.SetTrigger("IsIdle");
                break;
            default:
                break;
        }
    }
    void ImMyOwnBoss()
    {
        if (Input.GetKeyDown(KeyCode.W) && Input.GetKeyDown(KeyCode.Q))
        {
            switch (Random.Range(0, 2))
            {
                case 1:
                    anim.SetTrigger("IsFlip");
                    break;

                case 2:
                    anim.SetTrigger("IsIdle");
                    break;

                default:
                    break;
            }
        }
        else if (elapsedTime > Random.Range(5, 7))
        {
            switch (Random.Range(0, 2))
            {
                case 1:
                    anim.SetTrigger("IsFlip");
                    break;

                case 2:
                    anim.SetTrigger("IsIdle");
                    break;

                default:
                    break;
            }
        }
    }
}
