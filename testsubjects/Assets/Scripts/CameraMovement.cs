﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    Vector3 oldPos;
    Vector3 panOrigin;
    Vector3 TargetPos;
    Vector3 initpos;

    [Range(0.0f, 1.0f)]
    public float sm = 1;

    public float height;
    public bool dragging = false;
    public Vector3 dropPosition;
    public DragManager drag;

    Vector3 GetWorldPos(string layermask)
    {
        RaycastHit hit;
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition, Camera.main.stereoActiveEye);
        int mask = LayerMask.GetMask(layermask);

        Physics.Raycast(ray, out hit, 10000, mask);

        return hit.point;
    }

    // Start is called before the first frame update
    void Start()
    {
        TargetPos = transform.position;
        height = transform.position.y;
        initpos = transform.position;
    }

    void LateUpdate()
    {

        if (Input.GetMouseButton(0))
        {
            DragAndDrop();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            dragging = false;
            drag.dragging = false;
        }
        else
            CameraDrag();

        height += -Input.mouseScrollDelta.y;

        if (height > 100)
            height = 100;

        else if (height < 10)
            height = 10;
        else
        {
            TargetPos += this.transform.forward * Input.mouseScrollDelta.y;
        }

        float t = 1.0f - Mathf.Pow(Mathf.Pow(1.0f - sm, 30f), Time.deltaTime);
        transform.position = new Vector3(   Mathf.Lerp(transform.position.x, TargetPos.x, t),
                                            Mathf.Lerp(transform.position.y, height, t),
                                            Mathf.Lerp(transform.position.z, TargetPos.z, t));

        oldPos = transform.position;

    }

    void CameraDrag()
    {
        if (Input.GetMouseButtonDown(1))
        {
            oldPos = transform.position;
            panOrigin = GetWorldPos("CameraDrag");
        }

        if (Input.GetMouseButton(1))
        {
            Vector3 pos = GetWorldPos("CameraDrag") - panOrigin;
            TargetPos = oldPos - pos;
            setvalidpos();
        }

        if (Input.GetMouseButtonUp(1))
        {
            RaycastHit hit;
            if (Physics.Raycast(this.transform.position, transform.forward, out hit))
            {
                if (hit.transform.name == "CameraDragLayer")
                {
                    TargetPos = new Vector3(initpos.x, height, initpos.z);
                }
            }
        }
    }

    void DragAndDrop()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition, Camera.main.stereoActiveEye);

            if (Physics.Raycast(ray, out hit) && hit.transform.tag == "Player")
            {
                dragging = true;
                drag.dragging = true;
                drag.DraggedObject = hit.transform.gameObject;
            }
        }

        if (dragging)
        {
            dropPosition = GetWorldPos("Floor");
            drag.DropPosition = dropPosition;
        }
    }

    void setvalidpos()
    {
        RaycastHit hit;
        if (Physics.Raycast(this.transform.position, transform.forward, out hit))
        {
            if (hit.transform.name != "CameraDragLayer")
            {
                initpos = transform.position;
            }
        }
    }
}
