﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragManager : MonoBehaviour
{
    public bool dragging = false;
    public Vector3 DropPosition;
    public GameObject DraggedObject;
    public ButtonBehavior ActivatedButton;
    public DraggedBehavior Dragger;
    public GameManager GM;

    // Start is called before the first frame update
    void Start()
    {
        GM = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            if (dragging && ActivatedButton != null && ActivatedButton.ActiveButton)
            {
                // do whatever with the button
                Dragger.OnUse(ActivatedButton);
                GM.OnGameObjectDeleted(DraggedObject);

                Debug.Log("SAYONARA BABY");
                ActivatedButton.ActiveButton = false;
                ActivatedButton.anim.SetTrigger("Used");
                dragging = false;
                DraggedObject = null;
            }
        }
    }
}
