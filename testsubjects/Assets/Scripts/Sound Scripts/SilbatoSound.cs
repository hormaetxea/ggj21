﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SilbatoSound : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioSource source;

    void Start()
    {
        source = GetComponentInChildren<AudioSource>();
    }

    void Update()
    {
        // When spacebar is hit
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // play the sound
            source.Play();
        }
    }

}
