﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DraggedBehavior : MonoBehaviour
{
    bool DraggingPlayer = false;
    Transform DraggedOriginalTransform;

    public DragManager drag;
    public Canvas canvas;
    // Start is called before the first frame update
    void Start()
    {
        canvas = GetComponentInParent<Canvas>();
    }

    // Update is called once per frame
    void Update()
    {
        if (drag.dragging && !DraggingPlayer)
        {
            OnDrag();
            DraggingPlayer = true;
        }

        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, Input.mousePosition, canvas.worldCamera, out pos);
        transform.position = canvas.transform.TransformPoint(pos);


        if (drag.dragging)
        {

        }
        else if (DraggingPlayer && drag.DraggedObject != null)
        {
            drag.DraggedObject.transform.SetParent(DraggedOriginalTransform);
            drag.DraggedObject.transform.position = drag.DropPosition;
            drag.DraggedObject.transform.localScale = new Vector3(1, 1, 1);
            drag.DraggedObject.GetComponent<PlayerMovement>().IsDragged = false;
            DraggingPlayer = false;
            DraggedOriginalTransform = null;
        }
    }

    private void OnDrag()
    {
        DraggedOriginalTransform = drag.DraggedObject.transform.parent;
        drag.DraggedObject.transform.SetParent(this.transform);
        drag.DraggedObject.transform.localScale *= this.transform.localScale.x;
        drag.DraggedObject.transform.localPosition = new Vector3(0, -drag.DraggedObject.transform.localScale.x, 0);
        drag.DraggedObject.GetComponent<PlayerMovement>().IsDragged = true;
    }

    public void OnUse(ButtonBehavior button)
    {
        // TRASHCAN
        if (button.type == 0)
        {
            drag.DraggedObject.transform.SetParent(DraggedOriginalTransform);
            drag.DraggedObject.GetComponent<PlayerMovement>().IsDragged = false;
            DraggingPlayer = false;
            DraggedOriginalTransform = null;
        }
    }
}
